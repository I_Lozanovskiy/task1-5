package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class QuestWithNatNum {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите начальное значение");
        int x = Integer.parseInt(reader.readLine());
        System.out.println("Введите конечное значение");
        int y = Integer.parseInt(reader.readLine());

        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = x; i <= y; i++) {
            if (nat(i)) {

                numbers.add(i);
            }
        }
        System.out.print(numbers);
    }

    static boolean nat(int a) {

        int p = 0;
        if (a==1){
            return false;
        }
        if (a==2){
            return true;
        }
        if (a==5){
            return true;
        }
        if ((a % 2 == 0) || (a % 10 == 5)) {//исключаем четные числа и те, которые заканчиваются на 5
            return false;
        } else {
            for (int j = 3; j <= Math.sqrt(a); j += 2) {//делим на все нечетные числа до корня из i
                if (a % j == 0) {// если хотя бы на одно число делится, то переход к следующему числу
                    p += 1;
                    break;
                }
            }
        }
        return p <= 0;
    }
}

