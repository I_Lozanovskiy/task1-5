package com.company;

import java.util.ArrayList;
import java.util.Scanner;


public class Basket {


    public static void main(String[] args) {
        ArrayList<String> List = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        System.out.println("Список команд: ");
        System.out.println("Очистка: clear ");
        System.out.println("Печать: print ");
        System.out.println("Добаление: add [...] ");
        while (sc.hasNext()) {
            String s = sc.nextLine();

            if (s.equals("exit")) {
                break;
            }

            String[] parts = s.split(" ");

            String comand = parts[0];


            if (comand.matches("add") && parts.length==2){
                String productName = parts[1];
                List.add(productName);
            }
            if (comand.equals("clear")) {
                List.clear();
                System.out.println("All clean");
            }
            if (comand.matches("print") && parts.length==1) {
                for (String temp : List) {
                    System.out.println(temp);
                }
            }

        }
    }
}