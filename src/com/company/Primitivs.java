package com.company;

public class Primitivs {
    public static void main(String[] args) {
        byte bmin = -128;
        byte bmax = 127;
        System.out.println(bmax);
        System.out.println(bmin);

        short smax = 32767;
        short smin = -32768;
        System.out.println(smax);
        System.out.println(smin);

        int imax = (int)Math.pow(-2,31);
        int imin = (int)Math.pow(2,31);
        System.out.println(imax);
        System.out.println(imin);

        long lmax = (long) Math.pow(-2,63);
        long lmin = (long) Math.pow(2,63);
        System.out.println(lmax);
        System.out.println(lmin);
        float f1 = (float) 3.4e-38;
        float f2 = (float) 3.4e38;
        System.out.println(f1);
        System.out.println(f2);

        double d1 = 1.7e-308;
        double d2 = 1.7e308;
        System.out.println(d1);
        System.out.println(d2);

        String s = "(-_-)";
        System.out.println(s);

        char c1 = 0;
        char c2 = 65535;
        System.out.println((int)c1);
        System.out.println((int)c2);

        int[] myArray = new int[4];
        myArray[0] = 1;
        myArray[1] = 2;
        myArray[2] = 3;
        myArray[3] = 4;
        for (int i = 0; i < 4; i++) {
            System.out.println(myArray[i]);
        }

        boolean t = true;
        boolean f = false;
        System.out.println(t);
        System.out.println(f);







    }

}
